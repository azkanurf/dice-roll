import React, {useState} from 'react';
import './App.css';

function App() {
  const [listPlayer, setListPlayer] = useState([]);
  const [inputName, setInputName] = useState("");
  const [dice, setDice] = useState([]);
  const [addDice, setAddDice] = useState();
  // const [PR, setPR] = useState({});
  //PR = Player's roll
  const [score, setScore] = useState([])
  const [maxdice, setMaxdice] = useState(0)
  const [isDisabled, setIsDisabled] = useState(false);

  const handleInputPlayer = (e) =>{
    e.preventDefault()
    setListPlayer([...listPlayer,inputName])
    setListPlayer(listPlayer.concat(inputName))
    // console.log(inputName);
    console.log(listPlayer, listPlayer.length);
  }

  const diceRoll = () => {
    console.log("dice was rolled")
    setAddDice(1 + Math.floor(Math.random() * 6))
    // console.log(addDice)
    setDice(dice.concat(addDice))
    if(addDice===1 || addDice===3 || addDice===5){
      setScore(5)
    }else if(addDice===0){
      setScore(0)
    }else{
      setScore(-2)
    }
    console.log(score)
  }

  const maxRoll = (e) => {
    setMaxdice(maxdice + 1)
    if(maxdice < 5){
      setIsDisabled(false)
    }else{
      setIsDisabled(true)
    }
    // console.log(isDisabled, maxdice)

  }

  return (
    <div className="App">
      <div className="input-player">
        <input type="text" placeholder="input name" value={inputName} onChange={e => setInputName(e.target.value)}></input>
        <button type="button" onClick={handleInputPlayer}>input</button>
      </div>
      <div className="players">
        {listPlayer.map((player, index)=> (
          <div className="player" id={"player" + index} key={index} >
            <p>{player}</p>
            <button id={index} onClick={() =>{diceRoll(); maxRoll();}} disabled={isDisabled}>roll the dice</button>
          </div>
        ))}
      </div>  
      <div className="dice-roll">
          {dice.map((dice) => (
            <>
              <p>{dice}</p>
            </>
          ))}
        </div>
    </div>

  );
}

export default App;
